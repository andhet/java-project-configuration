<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE module PUBLIC "-//Checkstyle//DTD Checkstyle Configuration 1.3//EN" "https://checkstyle.org/dtds/configuration_1_3.dtd">

<!--
    This configuration file was written by the eclipse-cs plugin configuration editor
-->
<!--
    Checkstyle-Configuration: andhet
    Description: none
-->
<module name="Checker">
  <property name="severity" value="warning"/>
  <module name="TreeWalker">
    <module name="MissingDeprecated"/>
    <module name="MissingOverride"/>
    <module name="AnnotationLocation"/>
    <module name="MissingJavadocPackage"/>
    <module name="JavadocType">
      <property name="tokens" value="INTERFACE_DEF,ENUM_DEF,CLASS_DEF,ANNOTATION_DEF"/>
    </module>
    <module name="JavadocVariable">
      <property name="scope" value="public"/>
    </module>
    <module name="JavadocStyle">
      <metadata name="net.sf.eclipsecs.core.comment" value="enforce period"/>
      <property name="checkEmptyJavadoc" value="true"/>
    </module>
    <module name="InvalidJavadocPosition"/>
    <module name="JavadocBlockTagLocation"/>
    <module name="JavadocMethod">
      <metadata name="net.sf.eclipsecs.core.comment" value="throw documentation needed"/>
      <property name="validateThrows" value="true"/>
    </module>
    <module name="JavadocParagraph"/>
    <module name="JavadocTagContinuationIndentation"/>
    <module name="MissingJavadocMethod">
      <metadata name="net.sf.eclipsecs.core.comment" value="ignore getter/setters"/>
      <property name="scope" value="protected"/>
      <property name="allowMissingPropertyJavadoc" value="true"/>
    </module>
    <module name="MissingJavadocType">
      <property name="scope" value="protected"/>
    </module>
    <module name="NonEmptyAtclauseDescription"/>
    <module name="JavadocContentLocation"/>
    <module name="AbstractClassName"/>
    <module name="CatchParameterName"/>
    <module name="ClassTypeParameterName">
      <metadata name="net.sf.eclipsecs.core.comment" value="max 2 chars"/>
      <property name="format" value="^[A-Z]{1,2}$"/>
    </module>
    <module name="ConstantName"/>
    <module name="InterfaceTypeParameterName">
      <metadata name="net.sf.eclipsecs.core.comment" value="max 2 chars"/>
      <property name="format" value="^[A-Z]{1,2}$"/>
    </module>
    <module name="LambdaParameterName"/>
    <module name="LocalFinalVariableName"/>
    <module name="LocalVariableName"/>
    <module name="MemberName"/>
    <module name="MethodName"/>
    <module name="MethodTypeParameterName">
      <metadata name="net.sf.eclipsecs.core.comment" value="max 2 chars"/>
      <property name="format" value="^[A-Z]{1,2}$"/>
    </module>
    <module name="PackageName"/>
    <module name="ParameterName">
      <property name="accessModifiers" value="public"/>
    </module>
    <module name="StaticVariableName"/>
    <module name="TypeName">
      <metadata name="net.sf.eclipsecs.core.comment" value="all execpet interfaces"/>
      <property name="tokens" value="CLASS_DEF,ENUM_DEF,ANNOTATION_DEF"/>
    </module>
    <module name="TypeName">
      <metadata name="net.sf.eclipsecs.core.comment" value="interfaces start with I -&gt; eclipse style"/>
      <property name="format" value="^I[A-Z][a-zA-Z0-9]*$"/>
      <property name="tokens" value="INTERFACE_DEF"/>
    </module>
    <module name="RedundantImport"/>
    <module name="UnusedImports"/>
    <module name="IllegalImport"/>
    <module name="AvoidStarImport">
      <property name="excludes" value="org.junit,org.hamcrest"/>
      <property name="allowClassImports" value="true"/>
      <property name="allowStaticMemberImports" value="true"/>
    </module>
    <module name="AnonInnerLength"/>
    <module name="ExecutableStatementCount">
      <property name="tokens" value="CTOR_DEF,METHOD_DEF,INSTANCE_INIT,STATIC_INIT"/>
    </module>
    <module name="MethodLength"/>
    <module name="ParameterNumber"/>
    <module name="EmptyLineSeparator">
      <property name="allowNoEmptyLineBetweenFields" value="true"/>
      <property name="tokens" value="PACKAGE_DEF,IMPORT,STATIC_IMPORT,CLASS_DEF,ENUM_DEF,INTERFACE_DEF,CTOR_DEF,METHOD_DEF,STATIC_INIT,INSTANCE_INIT,VARIABLE_DEF"/>
    </module>
    <module name="ModifierOrder"/>
    <module name="RedundantModifier">
      <metadata name="net.sf.eclipsecs.core.comment" value="improved readability"/>
    </module>
    <module name="EmptyBlock">
      <property name="option" value="text"/>
    </module>
    <module name="EmptyCatchBlock">
      <property name="exceptionVariableName" value="expected|ignore"/>
    </module>
    <module name="NeedBraces">
      <property name="tokens" value="LITERAL_DO,LITERAL_ELSE,LITERAL_IF,LITERAL_FOR,LITERAL_WHILE"/>
    </module>
    <module name="NeedBraces">
      <metadata name="net.sf.eclipsecs.core.comment" value="for Lambda"/>
      <property name="allowSingleLineStatement" value="true"/>
      <property name="tokens" value="LAMBDA"/>
    </module>
    <module name="AvoidNestedBlocks"/>
    <module name="AvoidNoArgumentSuperConstructorCall">
      <metadata name="net.sf.eclipsecs.core.comment" value="nobody needs these"/>
    </module>
    <module name="CovariantEquals"/>
    <module name="DefaultComesLast"/>
    <module name="DeclarationOrder"/>
    <module name="EmptyStatement"/>
    <module name="EqualsAvoidNull"/>
    <module name="EqualsHashCode"/>
    <module name="FallThrough"/>
    <module name="FinalLocalVariable"/>
    <module name="HiddenField">
      <metadata name="net.sf.eclipsecs.core.comment" value="setterCanReturnItsClass für Builder (withXYZ)"/>
      <property name="ignoreConstructorParameter" value="true"/>
      <property name="ignoreSetter" value="true"/>
      <property name="setterCanReturnItsClass" value="true"/>
      <property name="tokens" value="PARAMETER_DEF,VARIABLE_DEF,LAMBDA"/>
    </module>
    <module name="IllegalCatch"/>
    <module name="IllegalThrows"/>
    <module name="InnerAssignment"/>
    <module name="MissingSwitchDefault"/>
    <module name="ModifiedControlVariable">
      <metadata name="net.sf.eclipsecs.core.comment" value="in enhanced for-loop okay"/>
      <property name="skipEnhancedForLoopVariable" value="true"/>
    </module>
    <module name="MultipleVariableDeclarations"/>
    <module name="NestedForDepth">
      <property name="max" value="2"/>
    </module>
    <module name="NestedIfDepth">
      <property name="max" value="2"/>
    </module>
    <module name="NestedTryDepth">
      <property name="max" value="2"/>
    </module>
    <module name="NoClone"/>
    <module name="PackageDeclaration"/>
    <module name="NoFinalizer"/>
    <module name="OneStatementPerLine"/>
    <module name="OverloadMethodsDeclarationOrder">
      <metadata name="net.sf.eclipsecs.core.comment" value="improved readability"/>
    </module>
    <module name="RequireThis"/>
    <module name="SimplifyBooleanExpression"/>
    <module name="SimplifyBooleanReturn"/>
    <module name="StringLiteralEquality"/>
    <module name="SuperClone"/>
    <module name="SuperFinalize"/>
    <module name="UnnecessaryParentheses"/>
    <module name="UnnecessarySemicolonAfterTypeMemberDeclaration"/>
    <module name="UnnecessarySemicolonInEnumeration"/>
    <module name="UnnecessarySemicolonInTryWithResources"/>
    <module name="VariableDeclarationUsageDistance"/>
    <module name="FinalClass"/>
    <module name="HideUtilityClassConstructor"/>
    <module name="InnerTypeLast"/>
    <module name="InterfaceIsType"/>
    <module name="MutableException"/>
    <module name="BooleanExpressionComplexity"/>
    <module name="ClassDataAbstractionCoupling"/>
    <module name="ArrayTypeStyle"/>
    <module name="FinalParameters"/>
    <module name="TodoComment">
      <metadata name="net.sf.eclipsecs.core.comment" value="TODO"/>
      <property name="format" value="TODO"/>
    </module>
    <module name="TodoComment">
      <metadata name="net.sf.eclipsecs.core.comment" value="FIXME"/>
      <property name="format" value="FIXME"/>
    </module>
    <module name="TrailingComment">
      <property name="legalComment" value="\s?\$NON-NLS-\d+\$"/>
    </module>
    <module name="UpperEll"/>
    <module name="AvoidEscapedUnicodeCharacters"/>
    <module name="SuppressionCommentFilter">
      <metadata name="net.sf.eclipsecs.core.comment" value="CHECKSTYLE:OFF"/>
    </module>
    <module name="SuppressionCommentFilter">
      <metadata name="net.sf.eclipsecs.core.comment" value="Generated Code. Quelle unbekannt"/>
      <property name="offCommentFormat" value="BEGIN GENERATED CODE"/>
      <property name="onCommentFormat" value="END GENERATED CODE"/>
    </module>
    <module name="SuppressionCommentFilter">
      <metadata name="net.sf.eclipsecs.core.comment" value="CHECKSTYLE\.OFF:\s([\w\|]+), gezieltes Ausschalten"/>
      <property name="offCommentFormat" value="CHECKSTYLE\.OFF:\s([\w\|]+)"/>
      <property name="onCommentFormat" value="CHECKSTYLE\.ON:\s([\w\|]+)"/>
    </module>
    <module name="SuppressWithNearbyCommentFilter">
      <property name="commentFormat" value="CHECKSTYLE IGNORE (\w+) FOR (\d+) LINES"/>
      <property name="checkFormat" value="$1"/>
      <property name="influenceFormat" value="$2"/>
    </module>
    <module name="AvoidStaticImport">
      <property name="excludes" value="java.lang.Math"/>
    </module>
  </module>
  <module name="JavadocPackage"/>
  <module name="FileLength">
    <property name="max" value="999"/>
  </module>
  <module name="LineLength">
    <property name="max" value="120"/>
  </module>
  <module name="FileTabCharacter"/>
  <module name="UniqueProperties"/>
  <module name="SuppressionFilter">
    <property name="file" value="${config_loc}/SuppressionFilter.xml"/>
  </module>
</module>
